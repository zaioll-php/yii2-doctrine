<?php namespace Zaioll\YiiDoctrine;

use yii\base\Model;
use Doctrine\ORM\Events;
use yii\db\AfterSaveEvent;
use Doctrine\ORM\Event\PreUpdateEventArgs;

abstract class DataMapper extends Model
{
    const PRE_REMOVE    = Events::preRemove;

    const POST_REMOVE   = Events::postRemove;

    const PRE_PERSIST   = Events::prePersist;

    const POST_PERSIST  = Events::postPersist;

    const PRE_UPDATE    = Events::preUpdate;

    const POST_UPDATE   = Events::postUpdate;

    const POST_LOAD     = Events::postLoad;

    /**
     * @event Event an event that is triggered when the record is initialized via [[init()]].
     */
    const EVENT_INIT = 'init';
    /**
     * @event Event an event that is triggered after the record is created and populated with query result.
     */
    const EVENT_AFTER_FIND = 'afterFind';
    /**
     * @event ModelEvent an event that is triggered before inserting a record.
     * You may set [[ModelEvent::isValid]] to be `false` to stop the insertion.
     */
    const EVENT_BEFORE_INSERT = 'beforeInsert';
    /**
     * @event AfterSaveEvent an event that is triggered after a record is inserted.
     */
    const EVENT_AFTER_INSERT = 'afterInsert';
    /**
     * @event ModelEvent an event that is triggered before updating a record.
     * You may set [[ModelEvent::isValid]] to be `false` to stop the update.
     */
    const EVENT_BEFORE_UPDATE = 'beforeUpdate';
    /**
     * @event AfterSaveEvent an event that is triggered after a record is updated.
     */
    const EVENT_AFTER_UPDATE = 'afterUpdate';
    /**
     * @event ModelEvent an event that is triggered before deleting a record.
     * You may set [[ModelEvent::isValid]] to be `false` to stop the deletion.
     */
    const EVENT_BEFORE_DELETE = 'beforeDelete';
    /**
     * @event Event an event that is triggered after a record is deleted.
     */
    const EVENT_AFTER_DELETE = 'afterDelete';
    /**
     * @event Event an event that is triggered after a record is refreshed.
     * @since 2.0.8
     */
    const EVENT_AFTER_REFRESH = 'afterRefresh';

    /**
     * @var EntityManagerInterface
     */
    private static $em;

    /**
     * @var EventManager
     */
    private static $evm;

    /**
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        self::$em   = \Yii::$app->doctrine->entityManager;
        self::$evm  = \Yii::$app->doctrine->eventManager;

        parent::__construct($config); 
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();
        $this->trigger(self::EVENT_INIT);

        $this->registerEventListeners();
    }

    private function registerEventListeners()
    {
        self::$evm->addEventListener(
            [
                self::PRE_PERSIST,
                self::PRE_REMOVE,
                self::PRE_UPDATE,
                self::POST_LOAD,
                self::POST_PERSIST,
                self::POST_REMOVE,
                self::POST_UPDATE,
            ],
            $this
        );
    }

    /**
     * @return void
     */
    public function prePersist()
    {
        $dataMapperEvent = new DataMapperEvent;
        $this->trigger(self::EVENT_BEFORE_INSERT, $dataMapperEvent);

        return $dataMapperEvent->isValid;
    }

    public function postPersist()
    {
        $this->trigger(self::EVENT_AFTER_INSERT);
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs)
    {
        $dataMapperEvent = new DataMapperEvent;
        $this->trigger(self::EVENT_BEFORE_UPDATE, $dataMapperEvent);

        return $dataMapperEvent->isValid;
    }

    public function postUpdate()
    {
        $changedAttributes = [];
        $this->trigger(self::EVENT_AFTER_UPDATE, new AfterSaveEvent([
            'changeAttributes' => $changedAttributes
        ]));
    }

    public function preRemove()
    {
        $dataMapperEvent = new DataMapperEvent;
        $this->trigger(self::EVENT_BEFORE_DELETE, $dataMapperEvent);

        return $dataMapperEvent->isValid;
    }

    public function postRemove()
    {
        $this->trigger(self::EVENT_BEFORE_DELETE);
    }

    public function postLoad()
    {
        $this->trigger(self::EVENT_AFTER_FIND);
    }

    /**
     * @return Repository
     */
    public static function getRepository()
    {
        $class = basename(str_replace('\\', '/', static::class));
        return self::$em->getRepository($class);
    }

    /**
     * This method does not cause an immediate SQL insert. Call $em->flush() to applie it.
     *
     * @param bool $runValidation
     * @param mixed $attributeNames
     * @return bool
     */
    public function save($runValidation = true, array $attributeNames = null): bool
    {
        if ($runValidation && ! $this->validate($attributeNames)) {
            \Yii::info('Model not inserted due to validation error.', __METHOD__);
            return false;
        }
        self::$em->persist($this);
        return true;
    }
}
