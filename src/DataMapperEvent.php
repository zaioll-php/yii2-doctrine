<?php namespace Zaioll\YiiDoctrine;

use yii\base\Event;

class DataMapperEvent extends Event
{
    /**
     * @var bool whether the model is in valid status. Defaults to true.
     * A model is in valid status if it passes validations or certain checks.
     */
    public $isValid = true;
}
