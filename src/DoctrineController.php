<?php
namespace Zaioll\YiiDoctrine;

use yii\di\Instance;
use yii\base\Component;
use yii\console\Controller;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Helper\HelperSet;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputDefinition;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Doctrine\ORM\Tools\Console\ConsoleRunner as ORMConsole;
use Doctrine\Migrations\Tools\Console\Helper\ConfigurationHelper;
use Doctrine\Migrations\Tools\Console\ConsoleRunner as MigrationsConsole;
use Symfony\Component\Console\Input\StringInput;

/**
 * Class DoctrineController
 * @package Infinitiweb\YiiDoctrine\console
 */
class DoctrineController extends Controller
{
    /** @var string|Component */
    public $doctrine = 'doctrine';

    private $application;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();

        $this->doctrine = Instance::ensure($this->doctrine, Component::class);

        $this->application = new Application('Doctrine application');
        $this->application->setCatchExceptions(true);
        $this->application->setHelperSet($this->getHelperSet());

        ORMConsole::addCommands($this->application);
        MigrationsConsole::addCommands($this->application);
    }

    public function options($actionID)
    {
        return array_merge(parent::options($actionID), [
            'doctrine',
        ]);
    }

    /**
     * @param array $args
     * @throws \Doctrine\DBAL\Migrations\MigrationException
     */
    public function actionIndex(array $args = [])
    {
        $input = new StringInput(implode(' ', $args));

        $this->application->run($input);
    }

    /**
     * @throws \Doctrine\DBAL\Migrations\MigrationException
     */
    private function getHelperSet(): HelperSet
    {
        $helperSet = new HelperSet();

        $helperSet->set(new ConfigurationHelper(
            $this->doctrine->getConnection(),
            $this->doctrine->getMigrationsConfiguration()
        ));
        $helperSet->set(new ConnectionHelper($this->doctrine->getConnection()));
        $helperSet->set(new EntityManagerHelper($this->doctrine->getEntityManager()));
        $helperSet->set($helperSet->get('entityManager'), 'em');
        $helperSet->set(new QuestionHelper());

        return $helperSet;
    }
}
