<?php namespace Zaioll\YiiDoctrine;

use yii\base\Component;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use yii\caching\ApcCache as YiiApcCache;
use yii\caching\MemCache as YiiMemCache;
use yii\caching\FileCache as YiiFileCache;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Cache\ApcCache as DoctrineApcCache;
use Doctrine\Common\Cache\ApcuCache as DoctrineApcuCache;
use Doctrine\Common\Cache\ArrayCache as DoctrineArrayCache;
use Doctrine\Common\Cache\CacheProvider as DoctrineCacheProvider;
use Doctrine\Common\Cache\MemcacheCache as DoctrineMemcacheCache;
use Doctrine\Common\Cache\MemcachedCache as DoctrineMemcachedCache;
use Doctrine\Common\Cache\FilesystemCache as DoctrineFilesystemCache;
use Doctrine\Common\EventManager;
use Doctrine\Migrations\Configuration\Configuration as MigrationsConfiguration;
use RuntimeException;

/**
 * @see https://github.com/Infinitiweb/yii2-doctrine
 */
class Doctrine extends Component
{
    /**
     * @var string
     */
    private $proxyPath_;

    /**
     * @var bool
     */
    private $isDevMode_;

    /**
     * @var array
     */
    private $entityPath_ = ['@app/models/entities'];

    /**
     * @var EntityManagerInterface
     */
    private $entityManager_;

    /**
     * @var array
     */
    private $dbal_ = [];

    /**
     * @var string
     */
    private $migrationsNamespace_ = 'app\migrations';

    /**
     * @var string
     */
    private $customTemplate_;

    /**
     * @var string
     */
    private $columnName_ = 'version';

    /**
     * @var string
     */
    private $tableName_ = 'migrations';

    /**
     * @var string
     */
    private $migrationsPath_ = '@app/migrations';

    /**
     * @param array $config
     */
    public function __construct(array $config = [])
    {

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function getDbal(): array
    {
        return $this->dbal_;
    }

    /**
     * @param array $dbal
     * @return self
     */
    protected function setDbal(array $dbal): self
    {
        $this->dbal_ = $dbal;

        return $this;
    }

    /**
     * @return Connection
     */
    public function getConnection(): Connection
    {
        return $this->entityManager->getConnection();
    }

    /**
     * @return array
     */
    public function getEntityPath(): array
    {
        return $this->entityPath_;
    }

    /**
     * @return MigrationsConfiguration
     */
    public function getMigrationsConfiguration(): MigrationsConfiguration
    {
        $configuration = new MigrationsConfiguration($this->connection);
        $configuration->setMigrationsNamespace($this->migrationsNamespace);
        $configuration->setCustomTemplate($this->customTemplate);
        $configuration->setMigrationsTableName($this->tableName);
        $configuration->setMigrationsColumnName($this->columnName);
        $configuration->setMigrationsDirectory($this->migrationsPath);

        $configuration->setMigrationsAreOrganizedByYearAndMonth();

        return $configuration;
    }

    /**
     * @return string
     */
    public function getColumnName(): string
    {
        return $this->columnName_;
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->tableName_;
    }

    /**
     * @return string|null
     */
    public function getCustomTemplate(): ?string
    {
        return $this->customTemplate_;
    }

    /**
     * @return string
     */
    public function getMigrationsNamespace(): string
    {
        return $this->migrationsNamespace_;
    }

    /**
     * @param string $namespace
     * @return self
     */
    protected function setMigrationsNamespace(string $namespace): self
    {
        $this->migrationsNamespace_ = $namespace;

        return $this;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager_;
    }

    /**
     * Alias to get Entity Manager
     *
     * @return EntityManagerInterface
     */
    public function getEm(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManagerInterface $em
     * @return self
     */
    protected function setEntityManager(EntityManagerInterface $emgr): self
    {
        $this->entityManager_ = $emgr;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsDev(): bool
    {
        return $this->isDevMode_ ?? YII_ENV_DEV;
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();

        $this->migrationsPath = $this->createMigrationsDirectory($this->migrationsPath);
        $this->entityPath     = $this->createEntityDirectories($this->entityPath);

        AnnotationRegistry::registerAutoloadNamespace('Doctrine\ORM\Mapping', \Yii::getAlias('@vendor'));

        $config = $this->createConfig();
        $annotationDriver = $config->newDefaultAnnotationDriver($this->getEntityPath(), false);
        $config->setMetadataDriverImpl($annotationDriver);

        $this->entityManager = EntityManager::create($this->getDbal(), $config);
    }

    /**
     * @param array $directories
     * @return array
     */
    private function createEntityDirectories(array $directories): array
    {
        $entityDirectories = [];
        foreach ($directories as $path) {
            $path = \Yii::getAlias($path);
            $entityDirectories[] = $path;
            if (file_exists($path)) {
                continue;
            }
            if (! mkdir($path, 0765, true)) {
                throw new RuntimeException("Não foi possível criar o diretório '{$path}'!");
            }
        }
        return $entityDirectories;
    }

    /**
     * @param string $path
     * @throws RuntimeException
     * @return string
     */
    private function createMigrationsDirectory(string $path): string
    {
        $diretorio = \Yii::getAlias($path);
        if (file_exists($diretorio)) {
            return $diretorio;
        }
        if (! mkdir($diretorio, 0765, true)) {
            throw new RuntimeException("Não foi possível criar o diretório '{$path}'!");
        }
        return $diretorio;
    }

    /**
     * @return Configuration
     */
    private function createConfig(): Configuration
    {
        $cache = $this->createDoctrineCache();
        $config = new Configuration;

        $config->setMetadataCacheImpl($cache);
        $config->setQueryCacheImpl($cache);
        $config->setResultCacheImpl($cache);
        $config->setProxyDir($this->proxyPath);
        $config->setProxyNamespace('DoctrineProxies');
        $config->setAutoGenerateProxyClasses($this->isDev);

        return $config;
    }

    /**
     * @return DoctrineCacheProvider
     */
    private function createDoctrineCache(): DoctrineCacheProvider
    {
        $yiiCacheDriver = \Yii::$app->getCache();
        if ($yiiCacheDriver instanceof YiiApcCache) {
            $cache = $yiiCacheDriver->useApcu
                ? new DoctrineApcuCache
                : new DoctrineApcCache;
            $cache->setNamespace(sprintf('Doctrine_%s', md5($this->getProxyPath())));
            return $cache;
        }
        if ($yiiCacheDriver instanceof YiiFileCache) {
            $cache = new DoctrineFilesystemCache($yiiCacheDriver->cachePath);
            $cache->setNamespace(sprintf('Doctrine_%s', md5($this->proxyPath)));
            return $cache;
        }
        if ($yiiCacheDriver instanceof YiiMemCache) {
            $memcacheDriver = $yiiCacheDriver->getMemcache();
            if ($memcacheDriver instanceof \Memcache) {
                $cache = new DoctrineMemcacheCache();
                $cache->setMemcache($memcacheDriver);
                $cache->setNamespace(sprintf('Doctrine_%s', md5($this->proxyPath)));
                return $cache;
            }
            if ($memcacheDriver instanceof \Memcached) {
                $cache = new DoctrineMemcachedCache();
                $cache->setMemcached($memcacheDriver);
                $cache->setNamespace(sprintf('Doctrine_%s', md5($this->proxyPath)));
                return $cache;
            }
        }
        $cache = new DoctrineArrayCache;
        $cache->setNamespace(sprintf('Doctrine_%s', md5($this->proxyPath)));
        return $cache;
    }

    /**
     * @return string
     */
    public function getProxyPath(): string
    {
        return $this->proxyPath_ ?? \Yii::getAlias('@runtime/doctrine-proxies');
    }

    /**
     * Get the value of migrationsPath
     *
     * @return  string
     */
    public function getMigrationsPath(): string
    {
        return $this->migrationsPath_;
    }

    /**
     * Set the value of migrationsPath
     *
     * @param  string  $migrationsPath
     *
     * @return  self
     */
    protected function setMigrationsPath(string $migrationsPath)
    {
        $this->migrationsPath_ = $migrationsPath;

        return $this;
    }

    /**
     * @param string $entityPath
     * @return self
     */
    protected function setEntityPath(array $entityPath): self
    {
        $this->entityPath_ = $entityPath;

        return $this;
    }

    /**
     * Get the value of eventManager
     *
     * @return  EventManager
     */
    public function getEventManager()
    {
        return $this->entityManager->getEventManager();
    }

    /**
     * alias to get the value of eventManager
     *
     * @return  EventManager
     */
    public function getEvm()
    {
        return $this->eventManager;
    }
}
